import {List} from "immutable";

export default {
    projects: new List(),
    socials: new List(),
    isFetchingProjects: false,
    isFetchingSocials: false,
};